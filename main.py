import ifcfg
from Cocoa import (
    NSApplication,
    NSObject,
    NSStatusBar,
    NSVariableStatusItemLength,
    NSDate,
    NSLog,
    NSTimer,
    NSRunLoop,
    NSDefaultRunLoopMode
)
from PyObjCTools import AppHelper

start_time = NSDate.date()

class VPNStatusDelegate(NSObject):
    state = "idle"

    def applicationDidFinishLaunching_(self, sender):
        NSLog("Application started.")

        self.statusItem = (
            NSStatusBar.systemStatusBar().statusItemWithLength_(
                NSVariableStatusItemLength
            )
        )
        self.statusItem.setTitle_("VPN")
        self.statusItem.setHighlightMode_(True)
        self.statusItem.setEnabled_(False)

        self.timer = NSTimer.alloc().initWithFireDate_interval_target_selector_userInfo_repeats_(
            start_time, 5.0, self, "tick:", None, True
        )
        NSRunLoop.currentRunLoop().addTimer_forMode_(
            self.timer, NSDefaultRunLoopMode
        )
        self.timer.fire()

    def tick_(self, notification):
        if ifcfg.interfaces().get("utun2") is not None:
            self.statusItem.setEnabled_(True)
        else:
            self.statusItem.setEnabled_(False)


def main():
    app = NSApplication.sharedApplication()
    delegate = VPNStatusDelegate.alloc().init()
    app.setDelegate_(delegate)
    AppHelper.runEventLoop()


if __name__ == '__main__':
    main()
