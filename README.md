# Installation

Assuming you installed wireguard from [this method](https://trailofbits.github.io/algo/client-macos-wireguard.html)...

run:

```
cd $HOME
git clone https://gitlab.com/felipemocruha/vpn-widget
cd vpn-widget

virtualenv -p python3 .env
source .env/bin/activate
pip install -r requirements.txt
chmod +x vpn_widget.sh
sudo ln -s vpn.widget.plist /Library/LaunchAgents/vpn.widget.plist
```

This will make the app start on the next login.

If you can't wait, run:

```
launchctl load -w /Library/LaunchAgents/vpn.widget.plist
```


The script assumes your VPN interface will be `utun2`, if it's not, go and change it yourself with:

```
sed -i 's/utun2/your-interface-name/g' main.py
```

This is a terrible, very ugly hack. Good Luck!
